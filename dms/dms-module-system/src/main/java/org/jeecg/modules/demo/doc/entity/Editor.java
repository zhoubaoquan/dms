package org.jeecg.modules.demo.doc.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 模板word
 * @Author: jeecg-boot
 * @Date:   2020-04-04
 * @Version: V1.0
 */
@Data
@TableName("editor")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="editor对象", description="模板word")
public class Editor {
    
	/**主键id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键id")
	private String id;
	/**文档id*/
	@Excel(name = "文档id", width = 15)
    @ApiModelProperty(value = "文档id")
	private String docId;
	/**word模板*/
	@Excel(name = "word模板", width = 15)
    @ApiModelProperty(value = "word模板")
	private Object editor;
}
