package org.jeecg.modules.system.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.util.oConvertUtils;
import java.util.Date;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.modules.system.entity.DocHistoryCopy;
import org.jeecg.modules.system.service.IDocHistoryCopyService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

 /**
 * @Description: 历史数据存储
 * @Author: jeecg-boot
 * @Date:   2020-04-04
 * @Version: V1.0
 */
@Slf4j
@Api(tags="历史数据存储")
@RestController
@RequestMapping("/doc/docHistoryCopy")
public class DocHistoryCopyController extends JeecgController<DocHistoryCopy, IDocHistoryCopyService> {
	@Autowired
	private IDocHistoryCopyService docHistoryCopyService;
	
	/**
	 * 分页列表查询
	 *
	 * @param docHistoryCopy
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "历史数据存储-分页列表查询")
	@ApiOperation(value="历史数据存储-分页列表查询", notes="历史数据存储-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(DocHistoryCopy docHistoryCopy,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<DocHistoryCopy> queryWrapper = QueryGenerator.initQueryWrapper(docHistoryCopy, req.getParameterMap());
		Page<DocHistoryCopy> page = new Page<DocHistoryCopy>(pageNo, pageSize);
		IPage<DocHistoryCopy> pageList = docHistoryCopyService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 * 添加
	 *
	 * @param docHistoryCopy
	 * @return
	 */
	@AutoLog(value = "历史数据存储-添加")
	@ApiOperation(value="历史数据存储-添加", notes="历史数据存储-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody DocHistoryCopy docHistoryCopy) {
		docHistoryCopyService.save(docHistoryCopy);
		return Result.ok("添加成功！");
	}
	
	/**
	 * 编辑
	 *
	 * @param docHistoryCopy
	 * @return
	 */
	@AutoLog(value = "历史数据存储-编辑")
	@ApiOperation(value="历史数据存储-编辑", notes="历史数据存储-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody DocHistoryCopy docHistoryCopy) {
		docHistoryCopyService.updateById(docHistoryCopy);
		return Result.ok("编辑成功!");
	}
	
	/**
	 * 通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "历史数据存储-通过id删除")
	@ApiOperation(value="历史数据存储-通过id删除", notes="历史数据存储-通过id删除")
	@RequestMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		docHistoryCopyService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 * 批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "历史数据存储-批量删除")
	@ApiOperation(value="历史数据存储-批量删除", notes="历史数据存储-批量删除")
	@RequestMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.docHistoryCopyService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "历史数据存储-通过id查询")
	@ApiOperation(value="历史数据存储-通过id查询", notes="历史数据存储-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		DocHistoryCopy docHistoryCopy = docHistoryCopyService.getById(id);
		return Result.ok(docHistoryCopy);
	}

  /**
   * 导出excel
   *
   * @param request
   * @param docHistoryCopy
   */
  @RequestMapping(value = "/exportXls")
  public ModelAndView exportXls(HttpServletRequest request, DocHistoryCopy docHistoryCopy) {
      return super.exportXls(request, docHistoryCopy, DocHistoryCopy.class, "历史数据存储");
  }

  /**
   * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
  public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      return super.importExcel(request, response, DocHistoryCopy.class);
  }

}
