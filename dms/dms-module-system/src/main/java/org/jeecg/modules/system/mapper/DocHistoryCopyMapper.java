package org.jeecg.modules.system.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.system.entity.DocHistoryCopy;

/**
 * @Description: 历史数据存储
 * @Author: jeecg-boot
 * @Date:   2020-04-04
 * @Version: V1.0
 */
public interface DocHistoryCopyMapper extends BaseMapper<DocHistoryCopy> {

}
