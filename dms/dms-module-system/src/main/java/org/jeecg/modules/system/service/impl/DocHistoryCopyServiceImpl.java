package org.jeecg.modules.system.service.impl;
import org.jeecg.modules.system.entity.DocHistoryCopy;
import org.jeecg.modules.system.mapper.DocHistoryCopyMapper;
import org.jeecg.modules.system.service.IDocHistoryCopyService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 历史数据存储
 * @Author: jeecg-boot
 * @Date:   2020-04-04
 * @Version: V1.0
 */
@Service
public class DocHistoryCopyServiceImpl extends ServiceImpl<DocHistoryCopyMapper, DocHistoryCopy> implements IDocHistoryCopyService {

}
