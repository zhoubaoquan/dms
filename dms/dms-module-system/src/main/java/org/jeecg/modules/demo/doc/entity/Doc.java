package org.jeecg.modules.demo.doc.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 文档
 * @Author: jeecg-boot
 * @Date:   2020-04-02
 * @Version: V1.0
 */
@Data
@TableName("doc")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="doc对象", description="文档")
public class Doc implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
	@Excel(name = "创建人", width = 15)
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@Excel(name = "创建日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
	@Excel(name = "更新人", width = 15)
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@Excel(name = "更新日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/*@Excel(name = "更新日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")*/
	@ApiModelProperty(value = "修改")
	private java.util.Date upTime;
	/**所属部门*/
	@Excel(name = "所属部门", width = 15)
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**文件分类*/
	@Excel(name = "文件分类", width = 15, dictTable = "sys_category", dicText = "name", dicCode = "code")
	@Dict(dictTable = "sys_category", dicText = "name", dicCode = "code")
    @ApiModelProperty(value = "文件分类")
    private java.lang.String docCategory;
	private java.lang.String categoryName;
	/**文件名称*/
	@Excel(name = "文件名称", width = 15)
    @ApiModelProperty(value = "文件名称")
    private java.lang.String docName;
	/**文件编号*/
	@Excel(name = "文件编号", width = 15)
    @ApiModelProperty(value = "文件编号")
    private java.lang.String docNo;
	/**定制部门*/
	@Excel(name = "定制部门", width = 15)
    @ApiModelProperty(value = "定制部门")
    private java.lang.String dzDepartment;
	/**文件大小*/
	@Excel(name = "文件大小", width = 15)
    @ApiModelProperty(value = "文件大小")
    private java.math.BigDecimal docSize;
	/**JIC条款*/
	@Excel(name = "JIC条款", width = 15)
    @ApiModelProperty(value = "JIC条款")
    private java.lang.String jic;
	/**生效日期*/
	@Excel(name = "生效日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "生效日期")
    private java.util.Date sxTime;
	/**到期日期*/
	@Excel(name = "到期日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "到期日期")
    private java.util.Date dqTime;
	/**JIC*/
	@Excel(name = "JIC", width = 15, dicCode = "jic_status")
	@Dict(dicCode = "jic_status")
    @ApiModelProperty(value = "JIC")
    private java.lang.Integer isJic;
	/**能否下载*/
	@Excel(name = "能否下载", width = 15, dicCode = "print_status")
	@Dict(dicCode = "print_status")
    @ApiModelProperty(value = "能否下载")
    private java.lang.Integer download;
	/**能否打印*/
	@Excel(name = "能否打印", width = 15, dicCode = "download_status")
	@Dict(dicCode = "download_status")
    @ApiModelProperty(value = "能否打印")
    private java.lang.Integer print;
	/**审核人*/
	@Excel(name = "审核人", width = 15)
    @ApiModelProperty(value = "审核人")
    private java.lang.String audit;
	/**富文本*/
	@Excel(name = "富文本", width = 15)
    @ApiModelProperty(value = "富文本")
    private java.lang.String editor;
	/**文档状态
	 * 0:启用  1：禁用*/
	@Excel(name = "文档状态", width = 15, dicCode = "doc_status")
	@Dict(dicCode = "doc_status")
    @ApiModelProperty(value = "文档状态")
    private java.lang.String docStatus;
	/**是否过期*/
	@Excel(name = "是否过期", width = 15, dicCode = "doc_expired")
	@Dict(dicCode = "doc_expired")
    @ApiModelProperty(value = "是否过期")
    private java.lang.String docExpired;
	/**排序*/
	private java.lang.Integer sort;
	/**点击量*/
	private java.lang.Integer readCount;
	private java.lang.String pid;
	private java.lang.String fileKk;
}
