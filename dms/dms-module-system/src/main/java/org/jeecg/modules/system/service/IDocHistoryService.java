package org.jeecg.modules.system.service;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.system.entity.DocHistory;

/**
 * @Description: 操作记录
 * @Author: jeecg-boot
 * @Date:   2020-04-04
 * @Version: V1.0
 */
public interface IDocHistoryService extends IService<DocHistory> {

}
