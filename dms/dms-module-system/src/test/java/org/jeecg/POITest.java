package org.jeecg;

import org.apache.poi.POIXMLDocumentPart;
import org.apache.poi.POIXMLProperties.CoreProperties;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class POITest {

    @Test
    public void testPOI() throws Exception {
        FileOutputStream WPSfile = new FileOutputStream("/Users/zbq/POITest/example.doc");
        WPSfile.close();
    }

    @Test
    public void readContent() throws Exception {
//        File is = new File("/Users/zbq/POITest/市一院测试文档.docx");
        File is = new File("/Users/zbq/POITest/POITest2.docx");
        FileInputStream fis = new FileInputStream(is);
        XWPFDocument docx = new XWPFDocument(fis);
        List<XWPFParagraph> paragraphs = docx.getParagraphs();
        List<IBodyElement> bodyElements = docx.getBodyElements();
        /**
         * IBodyElement -> paragraph段落 -> part -> document -> footnoteText脚注
         */
//        for (int i = 0; i < bodyElements.size(); i++) {
//            bodyElements.get(i);
//        }

//        File is1 = new File("/Users/zbq/POITest/POITest2.docx");
//        FileInputStream fis1 = new FileInputStream(is1);
//        XWPFDocument docx1 = new XWPFDocument(fis1);

        /**
         * XWPFParagraph -> XWPFRuns -> XWPFRun
         * Run是最小单位
         *
         * paragraphs[x] -> part -> headerFooterPolicy -> defaultPageHeader(first/even) -> tables[x] -> text
         * 获取页眉表格内容
         */
//        for (XWPFParagraph para : paragraphs) {
//            List<XWPFRun> runs = para.getRuns();
//            runs.forEach((item)->{
//                System.out.println(item.getText(0) + "------------item");
//
//            });
//
//        }
//        for(int i=0;i<paragraphs.size();i++){
//            XWPFParagraph para = paragraphs.get(i);
//            docx1.setParagraph(para, i);
//        }
//        OutputStream os = new FileOutputStream("/Users/zbq/POITest/POITest2.docx");
//        docx.write(os);
    }

    @Test
    public void testCreate() throws IOException, XmlException {
        File is = new File("/Users/zbq/POITest/制度页眉.docx");//文件路径
        FileInputStream fis = new FileInputStream(is);
        XWPFDocument docx = new XWPFDocument(fis);//文档对象

//        FileInputStream fis1 = new FileInputStream("/Users/zbq/POITest/taget.doc");
//        XWPFDocument docx1 = new XWPFDocument(fis1);//文档对象
//        XWPFParagraph paragraph = docx1.createParagraph();

//        CTP ctp = CTP.Factory.newInstance();
//        XWPFParagraph paragraph = new XWPFParagraph(ctp, docx);//段落对象

//        String zdName = "测试";
//        String zdNO = "10086";
//        String page = "2/3";
//        ctp.addNewR().addNewT().setStringValue("\t制度名称：" + zdName + "\t制度编号：" + zdNO + "\t页码：" + page);//设置页眉参数

//        ctp.addNewR().addNewT().setSpace(SpaceAttribute.Space.PRESERVE);

//        CTSectPr sectPr = docx.getDocument().getBody().isSetSectPr() ? docx.getDocument().getBody().getSectPr() : docx.getDocument().getBody().addNewSectPr();
//        XWPFHeaderFooterPolicy policy = new XWPFHeaderFooterPolicy(docx, sectPr);
//        STHdrFtr.Enum first = STHdrFtr.FIRST;
//        first.toString();
//        docx.setParagraph(paragraph,1);

//        XWPFHeader header = policy.createHeader(STHdrFtr.DEFAULT, new XWPFParagraph[]{paragraph});

//        XWPFHeader header = policy.createHeader(XWPFHeaderFooterPolicy.DEFAULT);
//        XWPFHeader header1 = policy.createHeader(XWPFHeaderFooterPolicy.FIRST);

//        header.setXWPFDocument(docx);


        OutputStream os = new FileOutputStream("/Users/zbq/POITest/POITest2.docx");
        docx.write(os);//输出到本地
    }

    /**
     * 替换段落里面的变量
     *
     * @param doc    要替换的文档
     * @param params 参数
     */
    private void replaceInPara(XWPFDocument doc, Map<String, Object> params) {
        Iterator<XWPFParagraph> iterator = doc.getParagraphsIterator();
        XWPFParagraph para;
        while (iterator.hasNext()) {
            para = iterator.next();
            this.replaceInPara(para, params);
        }
    }

    /**
     * 替换段落里面的变量
     *
     * @param para   要替换的段落
     * @param params 参数
     */
    private void replaceInPara(XWPFParagraph para, Map<String, Object> params) {
        List<XWPFRun> runs;
        Matcher matcher;
        if (this.matcher(para.getParagraphText()).find()) {
            runs = para.getRuns();
            for (int i = 0; i < runs.size(); i++) {
                XWPFRun run = runs.get(i);
                String runText = run.toString();
                matcher = this.matcher(runText);
                if (matcher.find()) {
                    while ((matcher = this.matcher(runText)).find()) {
                        runText = matcher.replaceFirst(String.valueOf(params.get(matcher.group(1))));
                    }
                    // 直接调用XWPFRun的setText()方法设置文本时，在底层会重新创建一个XWPFRun，把文本附加在当前文本后面，
                    // 所以我们不能直接设值，需要先删除当前run,然后再自己手动插入一个新的run。
                    para.removeRun(i);
                    if (runText.equals("null")) {
                        runText = "";
                    }
                    para.insertNewRun(i).setText(runText);
                }
            }
        }
    }

    /**
     * 替换表格里面的变量
     *
     * @param doc    要替换的文档
     * @param params 参数
     */
    private void replaceInTable(XWPFDocument doc, Map<String, Object> params) {
        Iterator<XWPFTable> iterator = doc.getTablesIterator();
        XWPFTable table;
        List<XWPFTableRow> rows;
        List<XWPFTableCell> cells;
        List<XWPFParagraph> paras;
        while (iterator.hasNext()) {
            table = iterator.next();
            rows = table.getRows();
            for (XWPFTableRow row : rows) {
                cells = row.getTableCells();
                for (XWPFTableCell cell : cells) {

                    String cellTextString = cell.getText();
                    for (Entry<String, Object> e : params.entrySet()) {
                        if (cellTextString.contains("${" + e.getKey() + "}"))
                            cellTextString = cellTextString.replace("${" + e.getKey() + "}", e.getValue().toString());
                    }
                    cell.removeParagraph(0);
                    if (cellTextString.contains("${") && cellTextString.contains("}")) {
                        cellTextString = "";
                    }
                    cell.setText(cellTextString);
//                    paras = cell.getParagraphs();
//					for (XWPFParagraph para : paras) {
//						this.replaceInPara(para, params);
//					}

                }
            }
        }
    }

    /**
     * 正则匹配字符串
     *
     * @param str
     * @return
     */
    private Matcher matcher(String str) {
        Pattern pattern = Pattern.compile("\\$\\{(.+?)\\}", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(str);
        return matcher;
    }

    /**
     * 关闭输入流
     *
     * @param is
     */
    private void close(InputStream is) {
        if (is != null) {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 关闭输出流
     *
     * @param os
     */
    private void close(OutputStream os) {
        if (os != null) {
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 输出CoreProperties信息
     *
     * @param coreProps
     */
    private void printCoreProperties(CoreProperties coreProps) {
        System.out.println(coreProps.getCategory()); // 分类
        System.out.println(coreProps.getCreator()); // 创建者
        System.out.println(coreProps.getCreated()); // 创建时间
        System.out.println(coreProps.getTitle()); // 标题
    }

}
