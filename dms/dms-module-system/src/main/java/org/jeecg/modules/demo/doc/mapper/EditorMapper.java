package org.jeecg.modules.demo.doc.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.demo.doc.entity.Editor;

/**
 * @Description: 模板word
 * @Author: jeecg-boot
 * @Date:   2020-04-04
 * @Version: V1.0
 */
public interface EditorMapper extends BaseMapper<Editor> {

}
