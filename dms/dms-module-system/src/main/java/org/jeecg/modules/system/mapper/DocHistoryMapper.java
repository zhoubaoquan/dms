package org.jeecg.modules.system.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.system.entity.DocHistory;

/**
 * @Description: 操作记录
 * @Author: jeecg-boot
 * @Date:   2020-04-04
 * @Version: V1.0
 */
public interface DocHistoryMapper extends BaseMapper<DocHistory> {

}
