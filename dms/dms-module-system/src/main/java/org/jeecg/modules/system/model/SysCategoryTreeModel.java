package org.jeecg.modules.system.model;

import org.jeecg.modules.system.entity.SysCategory;
import org.jeecg.modules.system.entity.SysDepart;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 *  分类字典表，存储树结构数据的实体类
 * <p>
 * 
 * @Author ck
 * @date  2020-03-31
 */
public class SysCategoryTreeModel implements Serializable{

    private static final long serialVersionUID = 1L;

    /** 对应SysCategory中的id字段,前端数据树中的key*/
    private String key;

    /** 对应SysCategory中的id字段,前端数据树中的value*/
    private String value;

    /** 对应Category表name字段,前端数据树中的title*/
    private String title;


    private boolean isLeaf;
    // 以下所有字段均与SysCategory相同

    private String id;

    private String pid;

    private String name;

    private String code;

    private String createBy;

    private Date createTime;

    private String updateBy;

    private Date updateTime;

    private String sysOrgCode;
    
    private String hasChild;

    private Integer status;

    private Integer rowIndex;
    
    private List<SysCategoryTreeModel> children = new ArrayList<>();


    /**
     * 将SysDepart对象转换成SysDepartTreeModel对象
     * @param sysCategory
     */
	public SysCategoryTreeModel(SysCategory sysCategory) {
		this.key = sysCategory.getId();
        this.value = sysCategory.getId();
        this.title = sysCategory.getName();
        this.id = sysCategory.getId();
        this.pid = sysCategory.getPid();
        this.name = sysCategory.getName();
        this.code = sysCategory.getCode();
        this.status = sysCategory.getStatus();
        this.createBy = sysCategory.getCreateBy();
        this.createTime = sysCategory.getCreateTime();
        this.updateBy = sysCategory.getUpdateBy();
        this.updateTime = sysCategory.getUpdateTime();
        this.hasChild = sysCategory.getHasChild();
        this.sysOrgCode = sysCategory.getSysOrgCode();
        this.rowIndex = sysCategory.getRowIndex();
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean getIsLeaf() {
        return isLeaf;
    }

    public void setIsLeaf(boolean isleaf) {
        this.isLeaf = isleaf;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getSysOrgCode() {
        return sysOrgCode;
    }

    public void setSysOrgCode(String sysOrgCode) {
        this.sysOrgCode = sysOrgCode;
    }

    public String getHasChild() {
        return hasChild;
    }

    public void setHasChild(String hasChild) {
        this.hasChild = hasChild;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(Integer rowIndex) {
        this.rowIndex = rowIndex;
    }

    public List<SysCategoryTreeModel> getChildren() {
        return children;
    }

    public void setChildren(List<SysCategoryTreeModel> children) {
        this.children = children;
    }

    /**
     * 重写equals方法
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()){
            return false;
        }
        SysCategoryTreeModel that = (SysCategoryTreeModel) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(pid, that.pid) &&
                Objects.equals(name, that.name) &&
                Objects.equals(code, that.code) &&
                Objects.equals(createBy, that.createBy) &&
                Objects.equals(createTime, that.createTime) &&
                Objects.equals(updateBy, that.updateBy) &&
                Objects.equals(updateTime, that.updateTime) &&
                Objects.equals(sysOrgCode, that.sysOrgCode) &&
                Objects.equals(hasChild, that.hasChild) &&
                Objects.equals(status, that.status) &&
                Objects.equals(rowIndex, that.rowIndex) &&
                Objects.equals(children, that.children);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, pid, name, code, createBy, createTime, updateBy, updateTime, sysOrgCode, hasChild, status, rowIndex, children);
    }
}
