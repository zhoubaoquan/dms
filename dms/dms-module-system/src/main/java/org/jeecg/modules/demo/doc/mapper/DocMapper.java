package org.jeecg.modules.demo.doc.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.demo.doc.entity.Doc;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 文档
 * @Author: jeecg-boot
 * @Date: 2020-04-02
 * @Version: V1.0
 */
public interface DocMapper extends BaseMapper<Doc> {
    @Select("SELECT COUNT(id) FROM doc")
    Integer docCount();

    @Select("SELECT COUNT(id) FROM doc where jic != 'NULL' and jic != ''")
    Integer jciCount();

    @Select("SELECT COUNT(doc_expired) FROM doc WHERE doc_expired = 1")
    Integer isExpired();

    @Select("SELECT SUM(read_count) FROM doc")
    Integer readCount();
}
