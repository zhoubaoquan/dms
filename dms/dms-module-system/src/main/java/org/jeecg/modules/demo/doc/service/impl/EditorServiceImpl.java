package org.jeecg.modules.demo.doc.service.impl;
import org.jeecg.modules.demo.doc.entity.Editor;
import org.jeecg.modules.demo.doc.mapper.EditorMapper;
import org.jeecg.modules.demo.doc.service.IEditorService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 模板word
 * @Author: jeecg-boot
 * @Date:   2020-04-04
 * @Version: V1.0
 */
@Service
public class EditorServiceImpl extends ServiceImpl<EditorMapper, Editor> implements IEditorService {

}
