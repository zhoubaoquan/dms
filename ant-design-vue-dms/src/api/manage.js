import { axios } from '@/utils/request'

const api = {
  user: '/api/user',
  role: '/api/role',
  service: '/api/service',
  permission: '/api/permission',
  permissionNoPager: '/api/permission/no-pager'
}

export default api

//post
export function postAction(url,parameter) {
  return axios({
    url: url,
    method:'post' ,
    data: parameter
  })
}

//post method= {post | put}
export function httpAction(url,parameter,method) {
  return axios({
    url: url,
    method:method ,
    data: parameter
  })
}

//put
export function putAction(url,parameter) {
  return axios({
    url: url,
    method:'put',
    data: parameter
  })
}

//get
export function getAction(url,parameter) {
  return axios({
    url: url,
    method: 'get',
    params: parameter
  })
}

//deleteAction
export function deleteAction(url,parameter) {
  return axios({
    url: url,
    method: 'delete',
    params: parameter
  })
}

export function getUserList(parameter) {
  return axios({
    url: api.user,
    method: 'get',
    params: parameter
  })
}

export function getRoleList(parameter) {
  return axios({
    url: api.role,
    method: 'get',
    params: parameter
  })
}

export function getServiceList(parameter) {
  return axios({
    url: api.service,
    method: 'get',
    params: parameter
  })
}

export function getPermissions(parameter) {
  return axios({
    url: api.permissionNoPager,
    method: 'get',
    params: parameter
  })
}

// id == 0 add     post
// id != 0 update  put
export function saveService(parameter) {
  return axios({
    url: api.service,
    method: parameter.id == 0 ? 'post' : 'put',
    data: parameter
  })
}

/**
 * 下载文件 用于excel导出
 * @param url
 * @param parameter
 * @returns {*}
 */
export function downFile(url,parameter){
  return axios({
    url: url,
    params: parameter,
    method:'get' ,
    responseType: 'blob'
  })
}
export function downFiles(url,parameter){
  return axios({
    url: url,
    params: parameter,
    method:'post' ,
    responseType: 'blob'
  })
}

/**
 * 获取文件访问路径
 * @param avatar
 * @param imgerver
 * @param str
 * @returns {*}
 */
export function getFileAccessHttpUrl(avatar,imgerver,subStr) {
  if(avatar && avatar.indexOf(subStr) != -1 ){
    return avatar;
  }else{
    return imgerver + "/" + avatar;
  }
}

export function handleExportDoc(docName,recordId) {
  //alert("handleExportDoc=>"+recordId)
  if(!docName || typeof docName != "string"){
    docName = "导出文件"
  }
  let param = {
    docName:docName,
    recordId:recordId
  }
  //console.log("导出参数",param)
  downFile('/doc/doc/exportDoc',param).then((data)=>{
    if (!data) {
      this.$message.warning("文件下载失败")
      return
    }
    if (typeof window.navigator.msSaveBlob !== 'undefined') {
      window.navigator.msSaveBlob(new Blob([data],{type: 'application/vnd.ms-word'}), docName+'.doc')
    }else{
      let url = window.URL.createObjectURL(new Blob([data],{type: 'application/vnd.ms-word'}))
      let link = document.createElement('a')
      link.style.display = 'none'
      link.href = url
      link.setAttribute('download', docName+'.doc')
      document.body.appendChild(link)
      link.click()
      document.body.removeChild(link); //下载完成移除元素
      window.URL.revokeObjectURL(url); //释放掉blob对象
    }
  })
}
export function handleExportDocs(docNames,recordIds) {
  //alert("handleExportDoc=>"+recordId)
  for (let i = 0; i <docNames.length ; i++) {
    let docName = docNames[i]
    if (docName==null||docName=="") {
      docNames[i] = "导出文件"
    }
  }
  let param = {
    docName:docNames.toString(),
    recordId:recordIds.toString()
  }
  //console.log("导出参数",param)
  downFiles('/doc/doc/exportDocs',param).then((data)=>{
    if (!data) {
      this.$message.warning("文件下载失败")
      return
    }
    if (typeof window.navigator.msSaveBlob !== 'undefined') {
      for (let i = 0; i <docNames.length ; i++) {
        let docName=docNames[i]
        window.navigator.msSaveBlob(new Blob([data],{type: 'application/vnd.ms-word'}), docName+'.doc')
      }
    }else{
      for (let i = 0; i <docNames.length ; i++) {
      let url = window.URL.createObjectURL(new Blob([data],{type: 'application/vnd.ms-word'}))
        console.log(data)
      let link = document.createElement('a')
      link.style.display = 'none'
      link.href = url
        let docName=docNames[i]
        link.setAttribute('download', docName+'.doc')
        document.body.appendChild(link)
        link.click()
        document.body.removeChild(link); //下载完成移除元素
        window.URL.revokeObjectURL(url); //释放掉blob对象
      }
    }
  })
}
export function handleExportPDF(docName,recordId) {
  //alert("handleExportPDF=>"+recordId)
  if(!docName || typeof docName != "string"){
    docName = "导出文件"
  }
  let param = {
    docName:docName,
    recordId:recordId
  }
  //console.log("导出参数",param)
  downFile('/doc/doc/exportPDF',param).then((data)=>{
    if (!data) {
      this.$message.warning("文件下载失败")
      return
    }
    if (typeof window.navigator.msSaveBlob !== 'undefined') {
      window.navigator.msSaveBlob(new Blob([data],{type: 'application/pdf'}), docName+'.pdf')
    }else{
      let url = window.URL.createObjectURL(new Blob([data],{type: 'application/pdf'}))
      let link = document.createElement('a')
      link.style.display = 'none'
      link.href = url
      link.setAttribute('download', docName+'.pdf')
      document.body.appendChild(link)
      link.click()
      document.body.removeChild(link); //下载完成移除元素
      window.URL.revokeObjectURL(url); //释放掉blob对象
    }
  })
}
