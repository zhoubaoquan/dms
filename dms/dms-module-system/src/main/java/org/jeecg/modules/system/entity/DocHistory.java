package org.jeecg.modules.system.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 操作记录
 * @Author: jeecg-boot
 * @Date:   2020-04-04
 * @Version: V1.0
 */
@Data
@TableName("doc_history")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="doc_history对象", description="操作记录")
public class DocHistory {
    
	/**文档id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "文档id")
	private String id;
	/**文档名称*/
	@Excel(name = "文档名称", width = 15)
    @ApiModelProperty(value = "文档名称")
	private String docName;
	/**操作人员*/
	@Excel(name = "操作人员", width = 15)
    @ApiModelProperty(value = "操作人员")
	private String operator;
	/**操作*/
	@Excel(name = "操作", width = 15)
    @ApiModelProperty(value = "操作")
	private String operation;
	/**操作时间*/
	@Excel(name = "操作时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "操作时间")
	private Date operationTime;
	/**操作历史记录id*/
	@Excel(name = "操作历史记录id", width = 15)
    @ApiModelProperty(value = "操作历史记录id")
	private String historyId;
}
