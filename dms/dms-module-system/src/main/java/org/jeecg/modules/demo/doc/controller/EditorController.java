package org.jeecg.modules.demo.doc.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.util.oConvertUtils;
import java.util.Date;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.modules.demo.doc.entity.Editor;
import org.jeecg.modules.demo.doc.service.IEditorService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

 /**
 * @Description: 模板word
 * @Author: jeecg-boot
 * @Date:   2020-04-04
 * @Version: V1.0
 */
@Slf4j
@Api(tags="模板word")
@RestController
@RequestMapping("/doc/editor")
public class EditorController extends JeecgController<Editor, IEditorService> {
	@Autowired
	private IEditorService editorService;
	
	/**
	 * 分页列表查询
	 *
	 * @param editor
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "模板word-分页列表查询")
	@ApiOperation(value="模板word-分页列表查询", notes="模板word-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(Editor editor,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<Editor> queryWrapper = QueryGenerator.initQueryWrapper(editor, req.getParameterMap());
		Page<Editor> page = new Page<Editor>(pageNo, pageSize);
		IPage<Editor> pageList = editorService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 * 添加
	 *
	 * @param editor
	 * @return
	 */
	@AutoLog(value = "模板word-添加")
	@ApiOperation(value="模板word-添加", notes="模板word-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody Editor editor) {
		editorService.save(editor);
		return Result.ok("添加成功！");
	}
	
	/**
	 * 编辑
	 *
	 * @param editor
	 * @return
	 */
	@AutoLog(value = "模板word-编辑")
	@ApiOperation(value="模板word-编辑", notes="模板word-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody Editor editor) {
		editorService.updateById(editor);
		return Result.ok("编辑成功!");
	}
	
	/**
	 * 通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "模板word-通过id删除")
	@ApiOperation(value="模板word-通过id删除", notes="模板word-通过id删除")
	@RequestMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		editorService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 * 批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "模板word-批量删除")
	@ApiOperation(value="模板word-批量删除", notes="模板word-批量删除")
	@RequestMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.editorService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "模板word-通过id查询")
	@ApiOperation(value="模板word-通过id查询", notes="模板word-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		Editor editor = editorService.getById(id);
		return Result.ok(editor);
	}

  /**
   * 导出excel
   *
   * @param request
   * @param editor
   */
  @RequestMapping(value = "/exportXls")
  public ModelAndView exportXls(HttpServletRequest request, Editor editor) {
      return super.exportXls(request, editor, Editor.class, "模板word");
  }

  /**
   * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
  public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      return super.importExcel(request, response, Editor.class);
  }

}
