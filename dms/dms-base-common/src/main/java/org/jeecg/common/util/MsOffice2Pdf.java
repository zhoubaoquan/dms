package org.jeecg.common.util;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;

import java.io.File;

public class MsOffice2Pdf {


 static final int wdDoNotSaveChanges = 0;// 不保存待定的更改。
     public static final int wdFormatPDF = 17;// word转PDF 格式
 public static final int ppSaveAsPDF = 32;// ppt 转PDF 格式



     /**
 11      * 将指定路径的word文档转换成指定路径的pdf文档
 12      * 此处路径为绝对路径
 13     * @Title: word2PDF
 14     * @Description: TODO(这里用一句话描述这个方法的作用)
 15     * @param inputFile
 16     * @param pdfFile
 17     * @return void    返回类型
 18     * @author 尚晓飞
 19     * @date 2014-8-15 上午10:25:47
 20      */
     public static void word2PDF(String inputFile,String pdfFile){
         System.out.println("启动Word");
         long start = System.currentTimeMillis();
         ActiveXComponent app = null;
         try {
             app = new ActiveXComponent("Word.Application");
             app.setProperty("Visible", false);

             Dispatch docs = app.getProperty("Documents").toDispatch();
            System.out.println("打开文档" +inputFile);
              Dispatch doc = Dispatch.call(docs,//
                      "Open", //
                      inputFile,// FileName
                     false,// ConfirmConversions
                     true // ReadOnly
                     ).toDispatch();

             System.out.println("转换文档到PDF " + pdfFile);
             File tofile = new File(pdfFile);
             if (tofile.exists()) {
                  tofile.delete();
             }
             Dispatch.call(doc,//
                     "SaveAs", //
                     pdfFile, // FileName
                    wdFormatPDF);

            Dispatch.call(doc, "Close", false);
            long end = System.currentTimeMillis();
             System.out.println("转换完成..用时：" + (end - start) + "ms.");
        } catch (Exception e) {
             System.out.println("========Error:文档转换失败：" + e.getMessage());
         } finally {
          if (app != null)
                 app.invoke("Quit", wdDoNotSaveChanges);
         }

     }


 }
