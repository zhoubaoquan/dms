package org.jeecg.modules.system.service;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.system.entity.DocHistoryCopy;

/**
 * @Description: 历史数据存储
 * @Author: jeecg-boot
 * @Date:   2020-04-04
 * @Version: V1.0
 */
public interface IDocHistoryCopyService extends IService<DocHistoryCopy> {

}
