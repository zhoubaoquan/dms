package org.jeecg.modules.system.entity;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 历史数据存储
 * @Author: jeecg-boot
 * @Date:   2020-04-04
 * @Version: V1.0
 */
@Data
@TableName("doc_copy")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="doc_copy对象", description="历史数据存储")
public class DocHistoryCopy {

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
	private String id;
	/**创建人*/
	@Excel(name = "创建人", width = 15)
    @ApiModelProperty(value = "创建人")
	private String createBy;
	/**创建日期*/
	@Excel(name = "创建日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
	private Date createTime;
	/**更新人*/
	@Excel(name = "更新人", width = 15)
    @ApiModelProperty(value = "更新人")
	private String updateBy;
	/**更新日期*/
	@Excel(name = "更新日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
	private Date updateTime;
	@Excel(name = "更新日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "更新日期")
	private Date upTime;
	/**所属部门*/
	@Excel(name = "所属部门", width = 15)
    @ApiModelProperty(value = "所属部门")
	private String sysOrgCode;
	/**categoryName*/
	@Excel(name = "categoryName", width = 15)
    @ApiModelProperty(value = "categoryName")
	private String categoryName;
	/**文件分类*/
	@Excel(name = "文件分类", width = 15)
    @ApiModelProperty(value = "文件分类")
	private String docCategory;
	/**文件名称*/
	@Excel(name = "文件名称", width = 15)
    @ApiModelProperty(value = "文件名称")
	private String docName;
	/**文件编号*/
	@Excel(name = "文件编号", width = 15)
    @ApiModelProperty(value = "文件编号")
	private String docNo;
	/**定制部门*/
	@Excel(name = "定制部门", width = 15)
    @ApiModelProperty(value = "定制部门")
	private String dzDepartment;
	/**文件大小*/
	@Excel(name = "文件大小", width = 15)
    @ApiModelProperty(value = "文件大小")
	private java.math.BigDecimal docSize;
	/**JIC条款*/
	@Excel(name = "JIC条款", width = 15)
    @ApiModelProperty(value = "JIC条款")
	private String jic;
	/**生效日期*/
	@Excel(name = "生效日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "生效日期")
	private Date sxTime;
	/**到期日期*/
	@Excel(name = "到期日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "到期日期")
	private Date dqTime;
	/**JIC*/
	@Excel(name = "JIC", width = 15)
    @ApiModelProperty(value = "JIC")
	private Integer isJic;
	/**能否下载*/
	@Excel(name = "能否下载", width = 15)
    @ApiModelProperty(value = "能否下载")
	private Integer download;
	/**能否打印*/
	@Excel(name = "能否打印", width = 15)
    @ApiModelProperty(value = "能否打印")
	private Integer print;
	/**审核人*/
	@Excel(name = "审核人", width = 15)
    @ApiModelProperty(value = "审核人")
	private String audit;
	/**富文本*/
	@Excel(name = "富文本", width = 15)
    @ApiModelProperty(value = "富文本")
	private Object editor;
	/**是否过期*/
	@Excel(name = "是否过期", width = 15)
    @ApiModelProperty(value = "是否过期")
	private String docExpired;
	/**文档状态*/
	@Excel(name = "文档状态", width = 15)
    @ApiModelProperty(value = "文档状态")
	private String docStatus;
	private java.lang.String pid;
	private java.lang.String fileKk;
}
