package org.jeecg.modules.demo.doc.service;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.demo.doc.entity.Editor;

/**
 * @Description: 模板word
 * @Author: jeecg-boot
 * @Date:   2020-04-04
 * @Version: V1.0
 */
public interface IEditorService extends IService<Editor> {

}
