package org.jeecg.modules.demo.doc.service;

import org.jeecg.modules.demo.doc.entity.Doc;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: 文档
 * @Author: jeecg-boot
 * @Date:   2020-04-02
 * @Version: V1.0
 */
public interface IDocService extends IService<Doc> {
    /** 统计总文档量 */
    Integer docCount();

    /** 统计JCI文档总量 */
    Integer jciCount();

    /** 统计过期文档 */
    Integer isExpired();

    /** 统计总点击量 */
    Integer readCount();

    /** 查询单条 */
    Doc selectOne(String id);
}
