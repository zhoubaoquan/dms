package org.jeecg.modules.system.controller;

import org.jeecg.modules.system.service.ISysUserGuideService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ZBQ
 * @Description 用户手册controller
 * @createTime 2020-03-28 10:30
 */
@RestController
@RequestMapping("/Guide")
public class SysUserGuideController {

//    @Autowired
//    private ISysUserGuideService userGuideService;

    @GetMapping("/userGuide")
    public String userGuide() {
        return "用户手册";
    }
}
