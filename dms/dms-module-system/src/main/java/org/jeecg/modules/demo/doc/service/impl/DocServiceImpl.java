package org.jeecg.modules.demo.doc.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import net.sf.saxon.expr.instruct.ForEach;
import net.sf.saxon.expr.instruct.ForEachGroup;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.jeecg.modules.demo.doc.entity.Doc;
import org.jeecg.modules.demo.doc.mapper.DocMapper;
import org.jeecg.modules.demo.doc.service.IDocService;
import org.jeecg.modules.system.entity.SysCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

/**
 * @Description: 文档
 * @Author: jeecg-boot
 * @Date: 2020-04-02
 * @Version: V1.0
 */
@Service
public class DocServiceImpl extends ServiceImpl<DocMapper, Doc> implements IDocService {

    /**
     * 统计总文档量
     */
    @Override
    public Integer docCount() {
        return baseMapper.docCount();
    }

    /**
     * 统计JCI文档总量
     */
    @Override
    public Integer jciCount() {
        return baseMapper.jciCount();
    }

    /**
     * 统计过期文档
     */
    @Override
    public Integer isExpired() {
        return baseMapper.isExpired();
    }

    /**
     * 统计点击量
     *
     * @return
     */
    @Override
    public Integer readCount() {
        return baseMapper.readCount();
    }

    /**
     * 查询单条
     *
     * @param id
     */
    @Override
    public Doc selectOne(String id) {
        return baseMapper.selectById(id);
    }
}
